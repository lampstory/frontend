import React from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import './App.css';
import Head from "./components/Main/Head";
import Index from "./components/Pages/Index";
import Post from "./components/Pages/Post/Post";
import Profile from "./components/Pages/Profile";
import PostDetail from "./components/Pages/Post/PostDetail";

const createBrowserHistory = require("history").createBrowserHistory;
const history = createBrowserHistory();

function App() {
    return (
        <BrowserRouter history={history}>
            <div className="App">
                <Head />
                <div className={'Wrapper'}>
                    <Route exact path={'/'} component={Index}/>
                    <Route exact path={'/post'} component={Post}/>
                    <Route exact path={'/post/:post_id'} component={PostDetail}/>
                    <Route path={'/post/:post_id/:password'} component={PostDetail}/>
                    <Route path={'/profile'} component={Profile}/>
                </div>
            </div>
        </BrowserRouter>
    );
}

export default App;
