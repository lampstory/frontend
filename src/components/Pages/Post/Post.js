import React from 'react';
import PostCard from "./PostCard";
import Modal from "../../Main/Modal";

const base_url = `https://api.lampstory.site/v1/post/filters?is_draft=0&sort=fulldate_desc&limit=start`;

export default class Post extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            isLoading: true,
        };
    }

    componentDidMount() {
        fetch(base_url)
            .then(response => response.json())
            .then(data => this.setState({
                posts: data,
                isLoading: false
            }));
    }

    get_password = (e) => {
        e.preventDefault();
    }

    render() {
        const { posts, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>
        }

        return (
            <div>
                {(posts || []).map(post => (
                    <PostCard
                        post={post}
                        key={post.id}
                    />
                ))}
                {/*<Modal title={'My title'} data={(<p>123</p>)}/>*/}
            </div>
        )
    }
}
