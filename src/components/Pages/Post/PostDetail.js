import React from 'react';

export default class PostDetail extends React.Component {

    constructor(props) {
        super(props);
        this.password = props.match.params.password ? '/' + props.match.params.password : '';
        this.post_id = props.match.params.post_id;
        this.post_url = 'https://api.lampstory.site/v1/post/' + this.post_id + this.password;
        this.state = {
            post: null,
            status: null
        };
    }

    componentDidMount() {
        fetch(this.post_url)
            .then(response => response.json())
            .then(data => this.setState({
                post: data,
                status: data.status
            }));
    }

    render() {
        const { post, status } = this.state;

        if (status) {
            return <div className={'error'}>{post.message}</div>
        }

        return (
            <div>
                {this.state.post && (
                    <div>
                        <h1>{this.state.post.name}</h1>
                        <div>{this.state.post.fulldate} | {this.state.post.user} | {this.state.post.views}</div>
                        <div dangerouslySetInnerHTML={{__html: this.state.post.text}}/>
                    </div>
                )}
            </div>
        )
    }
}
