import React from 'react';
import {Link} from "react-router-dom";

export default class PostCard extends React.Component {

    constructor(props) {
        super(props);
        this.post = this.props.post;
        this.state = {
            id: null,
            showComponent: false,
        };
    }

    render() {
        return (
            <div className={'PostCard'}>
                {this.post.password ?
                    <div>{this.post.name} <b>Пароль</b></div>
                    :
                    <Link to={'/post/' + this.post.id}>{this.post.name} {this.post.password && (<b>Пароль</b>)}</Link>
                }
                <div>{this.post.fulldate}</div>
            </div>
        )
    }
}
