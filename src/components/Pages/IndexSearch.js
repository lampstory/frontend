import React from 'react';

class IndexSearch extends React.Component {

    render () {
        return (
            <div>
                {this.props.status && <p className="error">{this.props.status}</p>}
                {(this.props.data || []).map(item => (
                    <div className="ContentBoxCard">
                        Название: {item.name} Дата: {item.fulldate} Пользователь: {item.user} Пароль: {item.password ? 'Да' : 'Нет'}
                    </div>
                ))}
            </div>
        );
    };
}

export default IndexSearch;
