import React from 'react';
import IndexSearch from "./IndexSearch";

class Search extends React.Component {

    state = {
        status: null,
        list: null
    }

    get_posts = async (e) => {
        const search = e.currentTarget.value;
        if (search.trim()) {
            const url = await fetch(`https://api.lampstory.site/v1/post/filters?search=${search}`);
            const data = await url.json();
            if (data.status === 400) {
                this.setState({
                    list: null,
                    status: 'Сань, хуй соси'
                })
            } else {
                this.setState({
                    list: data,
                    status: null
                })
            }
        } else {
            this.setState({
                list: null,
                status: null
            })
        }
    };

    render () {
        return (
            <div className="aligner">
                <form className="Content">
                    <input
                        type="text"
                        onKeyUp={this.get_posts}
                        name="search"
                        placeholder="Тестовый стенд"
                        className="ContentSearch"
                    />
                    <IndexSearch
                        status={this.state.status}
                        data={this.state.list}
                    />
                </form>

            </div>
        );
    };
}

export default Search;
