import React from 'react';
import Logo from '../../logo.svg'
import {Link} from "react-router-dom";

class Head extends React.Component {
    render() {
        return (
            <header className="Head">
                <div className="aligner">
                    <div className="HeadContent">
                        <Link to={'/'} className={'HeadLogo'}>
                            <img className="Logo" src={Logo} />
                            Егортупой
                        </Link>
                        <nav className={'Navigation'}>
                            <Link to={'/post'} >Пост</Link>
                            <Link to={'/profile'} >Профиль</Link>
                        </nav>
                    </div>
                </div>
            </header>
        )
    }
}

export default Head;