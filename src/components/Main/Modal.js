import React from 'react';

export default class Modal extends React.Component {

    constructor(props) {
        super(props);
        this.title = props.title || 'None Title';
        this.data = props.data || 'None data';
        this.isOpen = props.isOpen || false;
    }

    render() {
        return (
            <React.Fragment>
                {this.isOpen &&
                    (<div className={'Modal'}>
                        <div className={'ModalBody'}>
                            <div>{this.title}</div>
                            <div>{this.data}</div>
                        </div>
                    </div>)
                }
            </React.Fragment>
        )
    }
}
